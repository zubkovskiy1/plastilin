var     gulp           = require('gulp'),
		gutil          = require('gulp-util' ),
//		sass           = require('gulp-sass'),
		browserSync    = require('browser-sync'),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify'),
		cleanCSS       = require('gulp-clean-css'),
		rename         = require('gulp-rename'),
		del            = require('del'),
		imagemin       = require('gulp-imagemin'),
		cache          = require('gulp-cache'),
		ftp            = require('vinyl-ftp'),
		notify         = require("gulp-notify"),
        less           = require('gulp-less'),
        path           = require('path'),
        autoprefixer   = require('gulp-autoprefixer');



// Скрипты проекта
gulp.task('scripts', function() {
	return gulp.src([
		'app/libs/jquery/dist/jquery.min.js',
		'app/libs/owl.carousel/dist/owl.carousel.js',
		])
	.pipe(concat('scripts.js'))
//	.pipe(uglify())
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('common', function() {
	return gulp.src([
		'app/js/common.js'
		])
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({stream: true}));
});



gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false
	});
});

//gulp.task('sass', function() {
//	return gulp.src('app/sass/**/*.scss')
//	.pipe(sass())
//	.pipe(autoprefixer(['last 15 versions']))
//	.pipe(gulp.dest('app/css'))
//	.pipe(browserSync.reload({stream: true}));
//});

gulp.task('less', function () {
  return gulp.src('app/less/**/*.less')
    .pipe(autoprefixer(['last 35 versions']))
//    .pipe(autoprefixer())
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('app/css'));
});



gulp.task('watch', ['less', 'scripts','common', 'browser-sync'], function() {
	gulp.watch('app/less/**/*.less', ['less']);
	gulp.watch(['libs/**/*.js', 'app/js/common.js'], ['scripts']);
	gulp.watch('app/*.html', browserSync.reload);
});

gulp.task('imagemin', function() {
	return gulp.src('app/img/**/*')
	.pipe(cache(imagemin()))
	.pipe(gulp.dest('dist/img')); 
});

gulp.task('build', ['removedist', 'imagemin', 'less', 'scripts', 'common'], function() {

	var buildFiles = gulp.src([
		'app/*.html',
		'app/.htaccess'
		]).pipe(gulp.dest('dist'));

    var buildFiles = gulp.src([
		'app/libs/**/*.*',
		'app/.htaccess'
		]).pipe(gulp.dest('dist/libs'));
    
    
	var buildCss = gulp.src([
		'app/css/style.css',
		]).pipe(gulp.dest('dist/css'));

	var buildJs = gulp.src([
		'app/js/scripts.js',
		'app/js/common.js'
		]).pipe(gulp.dest('dist/js'));

	var buildFonts = gulp.src([
		'app/fonts/**/*']
		).pipe(gulp.dest('dist/fonts'));

});

gulp.task('deploy', function() {

	var conn = ftp.create({
		host:      'hostname.com',
		user:      'username',
		password:  'userpassword',
		parallel:  10,
		log: gutil.log
	});

	var globs = [
	'dist/**',
	'dist/.htaccess',
	];
	return gulp.src(globs, {buffer: false})
	.pipe(conn.dest('/path/to/folder/on/server'));

});

gulp.task('removedist', function() { return del.sync('dist'); });
gulp.task('clearcache', function () { return cache.clearAll(); });

gulp.task('default', ['watch']);
