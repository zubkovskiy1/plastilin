$('document').ready(function () {

    $(".first__ilustration-item").not(":first").hide();
    $(".first__tab-item").not(":first").hide();
    $(".first-card__descr .first-card__tab").click(function () {
        $(".first-card__descr .first-card__tab").removeClass("active").eq($(this).index()).addClass("active");
        $(".first__tab-item").hide().eq($(this).index()).fadeIn()
        $(".first__ilustration-item").hide().eq($(this).index()).fadeIn()
    }).eq(0).addClass("active");
  
    
    $(".second__ilustration-item").not(":first").hide();
    $(".second__tab-item").not(":first").hide();
    $(".second-card__descr .second-card__tab").click(function () {
    
        $(".second-card__descr .second-card__tab").removeClass("active").eq($(this).index()).addClass("active");
        $(".second__tab-item").hide().eq($(this).index()).fadeIn()
        $(".second__ilustration-item").hide().eq($(this).index()).fadeIn()
    }).eq(0).addClass("active");
    
    
    
    
    $(".owl-carousel").owlCarousel({
        nav:true,
        responsive:{
            0:{
                items:1,
                nav:false,
            }
        } 
     });    
});